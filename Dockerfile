# Reference
# - [Minimize GO resource size with docker multi-stage](https://itnext.io/minimize-go-resource-size-with-docker-multi-stage-build-3b03cb9787e1)

##################################################
# BUILD Golang source files 
##################################################
# Choose a compiler OS
FROM golang:alpine AS builder

WORKDIR /workspace

# Copy all the source files
COPY . .

# Label stage for easy cleanup with following command
# - docker image prune --filter label=stage=builder -f
LABEL stage=builder

# Install any compiler-only dependencies
# - GOOS=linux (Disable cross platform cause it will be run with alpine)
# - CGO_ENABLED=0 (Disable dynamic linking with C system library. bui)
# RUN apk add --no-cache gcc libc-dev \
RUN \
  CGO_ENABLED=0 \
  GOOS=linux \
  go build \
    -a \
    -o ./build/app

##################################################
# BUILD Docker image
##################################################

# Runtime OS
FROM alpine AS final

ARG USERNAME=runner
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN \
    # Add required software
    apk add --no-cache \
      ca-certificates \
    \
    # Add optional software ( Removable)
    && apk add --no-cache \
      curl \
    \
    # Add non-root user
    && addgroup -S ${USERNAME} -g ${USER_GID} \
    && adduser -S ${USERNAME} -G ${USERNAME} -u ${USER_UID} -D 

WORKDIR /home/${USERNAME}

# Copy from builder the GO executable file
COPY --from=builder /workspace/build/ .

# Run as non-root user name "runner"
USER ${USERNAME}

# Execute the program upon start 
CMD [ "./app" ]