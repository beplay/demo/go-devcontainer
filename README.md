# Develop Go container application

## Resources

- [Minize GO resource size with Docker multi-stage build](https://itnext.io/minimize-go-resource-size-with-docker-multi-stage-build-3b03cb9787e1)

## Run with `docker`

```sh
docker build -t goapp .
docker run -it --rm --name goapp goapp

```

## Run with `docker-compose`

```sh
docker-compose up \
  --build \
  --force-recreate
```